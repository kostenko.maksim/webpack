<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/app.css?v=<?=filemtime('css/app.css');?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<header class="header"></header>
<main>
    <div class="form-phone">
        <div class="ajax-result">
            <form class="form">
                <span class="label-phone">Phone</span>
                <input id="phone" data-count="0" class="phone" type="text" autocomplete="tel">
                <button class="submit">Отправить</button>
            </form>
        </div>
        <small class="ajax__version"></small>
        <div><a class="open-modal" href="#openModal">Открыть модальное окно</a></div>
    </div>
</main>
<footer class="footer"></footer>
<div class="loader"></div>
<div class="modal__overall"></div>
<div class="modal__dialog">
    <div class="modal__content">
        <div class="modal__header">
            <h3 class="modal__title">Название</h3>
            <a href="#close" title="Close" class="close-modal">×</a>
        </div>
        <div class="modal__body">
            <p>Содержимое модального окна...</p>
        </div>
    </div>
</div>

<script src="js/app.js?v=<?=filemtime('js/app.js');?>"></script>

</body>
</html>