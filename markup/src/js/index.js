import $ from "jquery";
// Input mask
import "jquery.inputmask";

$(document).ready(function() {

    let $phone = $('#phone');
    $phone.inputmask({
        mask: "+7 (*99) 999-99-99",
        inputmode: "tel",
        showMaskOnHover: false,
        definitions: { '*': { "validator": "[9]" }}
    });

    $(document).on('click','.submit', function (event) {
        event.preventDefault();
        let postData = {},
            $phone = $('#phone'),
            count = $phone.data('count'),
            phoneValue = $phone.val().replace(/[^0-9.]/g, "");

        if (phoneValue.length == 0)
            return;

        postData.phone = phoneValue;
        postData.count = ++count;

        $('.loader').addClass('loader--active');

        $.ajax({
                method: "POST",
                data: postData
        }).done(function( html ) {
                //$('.ajax-result').html(html);
                $('.loader').removeClass('loader--active');
                $('.ajax__version').text('Данные обновлены с помощью ajax ver.'+postData.count);

                let $phone = $('#phone');
                $phone.data('count',postData.count);
                console.log($phone.val());
                // фиксим баг Inputmask и ajax когда мы передаем в поле 11 цифр
                // описание бага: вначало номера добавлялась 7 со смещением при каждом onAjaxSuccess
                if ($phone.val().length == 11)
                    $phone.val($phone.val().substr(1));

                $phone.inputmask({
                    mask: "+7 (*99) 999-99-99",
                    showMaskOnHover: false,
                    definitions: { '*': { "validator": "[9]" }}
                });
        });
    })

    $(document).on('click','.open-modal', function (event) {
        event.preventDefault();
        $('.modal__overall').toggleClass('modal__overall--show');
        $('.modal__dialog').show();
        $('body').css('overflow','hidden');
    });

    $(document).on('click','.modal__overall, .close-modal', function (event) {
        console.log('click');
        event.preventDefault();
        $('.modal__overall').toggleClass('modal__overall--show');
        $('.modal__dialog').hide();
        $('body').css('overflow','inherit');
    });

});