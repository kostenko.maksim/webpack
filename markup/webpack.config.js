const path = require('path');
const webpack = require("webpack");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

function _path(p) {
    return path.join(__dirname, p);
}

module.exports = {
    mode: 'development',
    entry: './src/index.js',

    output: {
        filename: '../../js/app.js',
        //path: path.resolve(__dirname, '../'),
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new MiniCssExtractPlugin({
            filename: '../../css/app.css',
        }),
    ],
    module: {
        rules: [
            {
                // JavaScript
                test: /\.js$/,
                loader: "babel-loader",
                exclude: "/node_modules/"
            },
            {
                // scss || sass
                test: /\.(scss|sass)$/i,
                use: [
                    //"style-loader",
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: "css-loader",
                        options: { sourceMap: true }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            sourceMap: true,
                            //config: { path: `./postcss.config.js` }
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: { sourceMap: true }
                    }

            ]
        }]
    },
    resolve: {
        alias: {
            'inputmask' : _path('node_modules/inputmask/dist/inputmask'),
            'jquery.inputmask' : _path('node_modules/inputmask/dist/jquery.inputmask'),
        },
    }

};